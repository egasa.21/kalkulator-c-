using System;

namespace kalkulator
{
    class kali : jumlah
    {
        private double angka1;
        private double angka2;

        public override double  setangka1(){
            Console.WriteLine("Masukan Angka pertama : ");
            angka1 = double.Parse(Console.ReadLine());
            return angka1;
        }

        public override double setangka2(){
            Console.WriteLine("Masukan Angka kedua : ");
            angka2 = double.Parse(Console.ReadLine());
            return angka2;
        }

        public double perkalian(){
            double kalii = angka1 * angka2;
            Console.WriteLine("Perkalian angka " + angka1 + " dan " + angka2  + " adalah" + kalii);
            return kalii;
        }
    }
}