using System;

namespace kalkulator
{
    class bagi : jumlah
    {
        private double angka1;
        private double angka2;

        public override double  setangka1(){
            Console.WriteLine("Masukan Angka pertama : ");
            angka1 = double.Parse(Console.ReadLine());
            return angka1;
        }

        public override double setangka2(){
            Console.WriteLine("Masukan Angka kedua : ");
            angka2 = double.Parse(Console.ReadLine());
            return angka2;
        }

        public double pembagian(){
            double bagii = angka1 / angka2;
            Console.WriteLine("Pembagian angka " + angka1 + " dan " + angka2  + " adalah" + bagii);
            return bagii;
        }
    }
}